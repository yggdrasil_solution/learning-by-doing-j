package FTP;

import cococare.common.ftp.CCFtp;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        //HP ALM, Aldon, Nexus, SMTP
        System.out.println(CCFtp.isConnectable("10.204.48.44", 8081, 3000));
        System.out.println(CCFtp.isConnectable("10.204.4.132", 446, 3000));
        System.out.println(CCFtp.isConnectable("localhost", 8081, 3000));
        System.out.println(CCFtp.isConnectable("smtp.gmail.com", 587, 5000));
    }
}