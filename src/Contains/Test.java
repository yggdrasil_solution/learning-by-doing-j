package Contains;

import java.util.Arrays;
import java.util.List;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        List<String> data = Arrays.asList("A", "B", "C");
        System.out.println(Arrays.asList("A", "B", "C").containsAll(data));
        System.out.println(Arrays.asList("A", "B").containsAll(data));
        System.out.println(Arrays.asList("A", "B", "D").containsAll(data));
    }
}