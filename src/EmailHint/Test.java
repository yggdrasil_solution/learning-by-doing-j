package EmailHint;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static String getEmailHint(String email) {
        return email.substring(0, 2)
                + email.substring(2, email.lastIndexOf(".") - 2).replaceAll(".", "*")
                + email.substring(email.lastIndexOf(".") - 2, email.length());
    }

    public static void main(String[] args) {
        String email1 = "arrow_405@yahoo.com";
        String email2 = "yosua.onesimus@gmail.com";
        System.out.println(email1);
        System.out.println(getEmailHint(email1));
        System.out.println(email2);
        System.out.println(getEmailHint(email2));
    }
}