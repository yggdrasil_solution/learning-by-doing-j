package MySqlPartition;

import cococare.common.CCClass;
import cococare.database.CCDatabase;
import cococare.database.CCDatabaseConfig;
import cococare.database.CCDatabaseDao;
import cococare.database.CCHibernate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        boolean createData = false;
        CCDatabaseConfig databaseConfig = new CCDatabaseConfig().
                withPassword("1234").
                withDatabase("MySqlPartition");
        CCHibernate hibernate = new CCHibernate();
        hibernate.addAnnotatedClass(Player.class);
        hibernate.addDatabaseConfig(databaseConfig, true);
        if (createData) {
            List<Player> players = new ArrayList();
            players.add(new Player("Yosua", "Swordman", "32"));
            players.add(new Player("Vanie", "Mage", "27"));
            players.add(new Player("Sharon", "Acolyte", "24"));
            players.add(new Player("Onesimus", "Mage", "32"));
            players.add(new Player("Euodia", "Thief", "27"));
            players.add(new Player("Angela", "Archer", "24"));
            players.add(new Player("Yosua-2", "Thief", "32"));
            players.add(new Player("Trivena", "Swordman", "27"));
            players.add(new Player("Ivana", "Merchant", "24"));
            System.out.println(hibernate.save(players));
        } else {
            String sql =
                    "SELECT T.* "
                    + "FROM ( "
                    + "  SELECT "
                    + "    @partition_id := CASE WHEN @partition_by = T.job THEN @partition_id + 1 ELSE 1 END AS partition_id, "
                    + "    @partition_by := T.job AS partition_by, "
                    + "    T.* "
                    + "  FROM ( "
                    + "    SELECT p.* "
                    + "    FROM players p "
                    + "    ORDER BY p.job ASC, p.level DESC, p.name ASC "
                    + "  ) AS T, (SELECT @partition_id := '', @partition_by := '') AS P "
                    + ") AS T "
                    + "WHERE partition_id = 1";
            final CCDatabase database = new CCDatabase();
            database.getConnection(databaseConfig, true);
            CCDatabaseDao databaseDao = new CCDatabaseDao() {
                @Override
                protected CCDatabase getCCDatabase() {
                    return database;
                }

                @Override
                protected Class getEntity() {
                    return Player.class;
                }
            };
            List<Player> players = databaseDao.getListBy(sql, null, null);
            for (Player player : players) {
                System.out.println(CCClass.getAssociativeArray(player, null));
            }
        }
    }
}