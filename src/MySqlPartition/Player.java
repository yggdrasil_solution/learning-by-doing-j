package MySqlPartition;

import cococare.common.CCFieldConfig;
import cococare.database.CCEntity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
@Entity
@Table(name = "players")
public class Player extends CCEntity {

    @Column(name = "name")
    @CCFieldConfig
    private String name;
    @Column(name = "job")
    @CCFieldConfig
    private String job;
    @Column(name = "level")
    @CCFieldConfig
    private String level;

    public Player() {
    }

    public Player(String name, String job, String level) {
        this.name = name;
        this.job = job;
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}