package PassByValue;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    private static void passByValue(
            int i1, int i2, int[] ii,
            Integer I1, Integer I2, Integer[] II,
            String s1, String s2, String[] ss) {
        i1 = 1;
        i2 = 1;
        ii[1] = 1;
        I1 = 1;
        I2 = 1;
        II[1] = 1;
        s1 = "A";
        s2 = "A";
        ss[1] = "A";
    }

    public static void main(String[] args) {
        int i = 0;
        int[] ii = new int[]{0, 0};
        Integer I = 0;
        Integer[] II = new Integer[]{0, 0};
        String s = "a";
        String[] ss = new String[]{"a", "a"};
        System.out.println(i + " " + ii[0] + " " + ii[1] + " " + I + " " + II[0] + " " + II[1] + " " + s + " " + ss[0] + " " + ss[1]);
        //0 0 0 0 0 0 a a a
        passByValue(
                i, ii[0], ii,
                I, II[0], II,
                s, ss[0], ss);
        System.out.println(i + " " + ii[0] + " " + ii[1] + " " + I + " " + II[0] + " " + II[1] + " " + s + " " + ss[0] + " " + ss[1]);
        //0 0 1 0 0 1 a a A
    }
}