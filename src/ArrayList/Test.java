package ArrayList;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        List<Person> persons = new ArrayList();
        persons.add(new Person("Yosua"));
        persons.add(new Person("Onesimus"));

        System.out.println("Nilai Awal:");
        for (Person person : persons) {
            System.out.println(person.getName());
        }

        System.out.println();
        for (Person person : persons) {
            person.setName(person.getName() + " edited");
        }

        System.out.println("Nilai Setelah Diubah:");
        for (Person person : persons) {
            System.out.println(person.getName());
        }
    }
}