package QandA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void test1() {
        String text = "\"xxx\", \"xxxxxx\", \"xxxxxx\", \"xxxxxx\", \"xx\", \"xxxx\"";
        char[] chars = text.toCharArray();
        text = "";
        int i = 0;
        for (char c : chars) {
            if (c == ',') {
                i++;
            }
            if (i > 3) {
                break;
            }
            text += c;
        }
        System.out.println(text);
    }

    public static void test2() {
        HashMap<String, List<String>> key_person = new HashMap();
        int autoIncrement = 1;
        List<List<String>> persons = new ArrayList();
        persons.add(new ArrayList(Arrays.asList("S200130001", "amin hadi", "solo", "1990-03-31", "571749475")));
        persons.add(new ArrayList(Arrays.asList("S200130002", "Zakia gotik", "Pekalongan", "1989-04-17", "571749476")));
        persons.add(new ArrayList(Arrays.asList("S200130003", "Romi Yanuar", "Rembang", "1990-02-21", "571749477")));
        persons.add(new ArrayList(Arrays.asList("S200130004", "Joko witono", "Semarang", "1979-07-16", "571749478")));
        persons.add(new ArrayList(Arrays.asList("S200130002", "amin hadi", "surakarta", "1990-03-31", "571749476")));
        for (List<String> person : persons) {
            String key = person.get(1) + person.get(3);
            List<String> p = key_person.get(key);
            if (p == null) {
                p = person;
                key_person.put(key, p);
            } else {
                String penanda = String.valueOf(autoIncrement);
                if (p.size() < 6) {
                    p.add(penanda);
                }
                if (person.size() < 6) {
                    person.add(p.get(5));
                }
                autoIncrement++;
            }
        }
        //output
        for (List<String> person : persons) {
            for (String data : person) {
                System.out.print(data + " ");
            }
            System.out.println("");
        }
    }

    public static void test3() {
        List<Integer> listA = new ArrayList(Arrays.asList(1, 2, 3, 4, 5, 6, 7));
        List<Integer> listB = new ArrayList(Arrays.asList(3, 4, 5, 6, 7, 8, 9));
        List<Integer> listO = new ArrayList();
        int a = 0;
        int b = 0;
        System.out.println("Size A, B, O: " + listA.size() + ", " + listB.size() + ", " + listO.size());
        loop:
        while (a < listA.size()) {
            while (b < listB.size()) {
                if (listA.get(a) == listB.get(b)) {
                    System.out.println("FOUND " + listA.get(a) + ", ADDED TO listO");
                    listO.add(listA.get(a));
                    System.out.println("REMOVE " + listA.get(a) + " FROM listA and list B");
                    listA.remove(a);
                    listB.remove(b);
                    a = 0;
                    b = 0;
                    continue loop;
                }
                b++;
            }
            b = 0;
            a++;
        }
        System.out.println("Size A, B, O: " + listA.size() + ", " + listB.size() + ", " + listO.size());
    }

    public static void test4() {
        //input
        //ada suatu list t dan map<integer, list<integer>>
        List<Integer> t = new ArrayList(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20));
        Map<Integer, List<Integer>> mapIntInts = new HashMap();
        mapIntInts.put(2, new ArrayList(Arrays.asList(2, 4, 6, 8, 10, 12, 14, 16, 18, 20)));
        mapIntInts.put(3, new ArrayList(Arrays.asList(3, 6, 9, 12, 15, 18)));
        mapIntInts.put(5, new ArrayList(Arrays.asList(5, 10, 15, 20)));
        //process
        for (Integer tItem : t) {
            //jika salah satu item di list t merupakan key dari map tersebut
            int key = tItem;
            List<Integer> ints = mapIntInts.get(key);
            if (ints != null) {
                for (Integer intsItem : ints) {
                    //jika intsItem sama dengan key, maka tidak ada yang perlu direplace
                    if (intsItem == key) {
                        continue;
                    }
                    //dan list t mengandung salah satu nilai dari list<integer>
                    int indexOfInt;
                    while ((indexOfInt = t.indexOf(intsItem)) > -1) {
                        //maka nilai itu akan direplace dengan nilai key-nya
                        t.set(indexOfInt, key);
                    }
                }
            }
        }
        //output
        for (Integer tItem : t) {
            System.out.println(tItem);
        }
    }

    public static void main(String[] args) {
        test4();
    }
}