package Reflection;

import cococare.common.CCClass;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class TestValue {

    private Integer I;
    private int i;
    private String S;
    private TestValue tv;

    public static void main(String[] args) {
        TestValue tv = new TestValue();
        CCClass.setValue(tv, "S", "S value");
        System.out.println(CCClass.getValue(tv, "S"));
        tv.tv = new TestValue();
        CCClass.setValue(tv, "tv.S", "tv S value");
        System.out.println(CCClass.getValue(tv, "tv.S"));
        CCClass.invoke(tv, "getTv.setS(?)", "tv S value");
        System.out.println(CCClass.getValue(tv, "tv.S"));
    }

    public String getS() {
        return S;
    }

    public void setS(String S) {
        this.S = S;
    }

    public TestValue getTv() {
        return tv;
    }

    public void setTv(TestValue tv) {
        this.tv = tv;
    }
}