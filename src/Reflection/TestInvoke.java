package Reflection;

import cococare.common.CCClass;
import cococare.common.CCConfig;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class TestInvoke {

    private Integer I;
    private Integer[] II;
    private int i;
    private int[] ii;
    private String S;
    private String[] SS;
    private TestInvoke ti;
    private TestInvoke[] titi;

    public Integer getI() {
        return I;
    }

    public void setI(Integer I) {
        this.I = I;
    }

    public Integer[] getII() {
        return II;
    }

    public void setII(Integer[] II) {
        this.II = II;
    }

    public int geti() {
        return i;
    }

    public void seti(int i) {
        this.i = i;
    }

    public int[] getIi() {
        return ii;
    }

    public void setIi(int[] ii) {
        this.ii = ii;
    }

    public String getS() {
        return S;
    }

    public void setS(String S) {
        this.S = S;
    }

    public String[] getSS() {
        return SS;
    }

    public void setSS(String[] SS) {
        this.SS = SS;
    }

    public TestInvoke getTi() {
        return ti;
    }

    public void setTi(TestInvoke ti) {
        this.ti = ti;
    }

    public TestInvoke[] getTiti() {
        return titi;
    }

    public void setTiti(TestInvoke[] titi) {
        this.titi = titi;
    }

    public void setIS(Integer I, String S) {
        this.I = I;
        this.S = S;
    }

    public static void main(String[] args) {
        CCConfig.MSG_SHOW_LOG_DEBUG = false;
        TestInvoke ti = new TestInvoke();
        CCClass.invoke(ti, "seti(?)", new CCClass.PrimitiveType(5));
        System.out.println(CCClass.invoke(ti, "geti()"));
        CCClass.invoke(ti, "setI(?)", 55);
        System.out.println(CCClass.invoke(ti, "getI()"));
        CCClass.invoke(ti, "setIS(?,?)", 555, "SSS");
        System.out.println(CCClass.invoke(ti, "getI"));
        System.out.println(CCClass.invoke(ti, "getS"));
        CCClass.invoke(ti, "setSS(?)", (Object) new String[]{"AAA", "BBB"});
        System.out.println(CCClass.invoke(ti, "getSS()[0]"));
        System.out.println(CCClass.invoke(ti, "getSS()[1]"));
        CCClass.invoke(ti, "getTi().setS(?)", "ti S value");
        System.out.println(CCClass.invoke(ti, "getTi().getS()"));
    }
}