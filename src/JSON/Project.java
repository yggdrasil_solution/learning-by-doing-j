package JSON;

import cococare.common.CCFieldConfig;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import java.util.List;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Project {

    @JsonView()
    @CCFieldConfig()
    private String application;
    @JsonView({JsonViewRule.Rule2.class})
    @JsonProperty("change_type")
    @CCFieldConfig()
    private String changeType;
    @JsonView({JsonViewRule.Rule1.class, JsonViewRule.Rule2.class})
    @CCFieldConfig()
    private String code;
    @JsonView({JsonViewRule.Rule1.class, JsonViewRule.Rule2.class})
    @CCFieldConfig()
    private String description;
    @JsonView({JsonViewRule.Rule2.class})
    @JsonProperty("person_in_charge")
    @CCFieldConfig()
    private String personInCharge;
    @JsonIgnore
    @CCFieldConfig(visible = false)
    private Boolean deleted = false;
    @JsonView({JsonViewRule.Rule2.class})
    @CCFieldConfig()
    transient private List<Task> tasks;

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getChangeType() {
        return changeType;
    }

    public void setChangeType(String changeType) {
        this.changeType = changeType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPersonInCharge() {
        return personInCharge;
    }

    public void setPersonInCharge(String personInCharge) {
        this.personInCharge = personInCharge;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}