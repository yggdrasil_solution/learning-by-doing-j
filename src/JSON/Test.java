package JSON;

import cococare.common.CCClass;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        try {
            Project project = new Project();
            project.setApplication("cms");
            project.setChangeType("production issue");
            project.setCode("1");
            project.setDescription("this is description");
            project.setPersonInCharge("Fajar MSS");

            Task task = new Task();
            task.setName("name_of_taskname");
            project.setTasks(Arrays.asList(task));

            File projectJson = new File("D:\\project.json");
            String projectJsonString;
            ObjectMapper objectMapper = new ObjectMapper();

            System.out.println();
            System.out.println("objectMapper.writeValueAsString(project)");
            System.out.println(projectJsonString = objectMapper.writeValueAsString(project));
            objectMapper.writeValue(projectJson, project);

            System.out.println();
            System.out.println("objectMapper.readValue(projectJson, Project.class)");
            System.out.println(CCClass.getAssociativeArray(objectMapper.readValue(projectJson, Project.class), null));
            System.out.println(CCClass.getAssociativeArray(objectMapper.readValue(projectJsonString, Project.class), null));

            System.out.println();
            System.out.println("objectMapper.writerWithView(JsonViewRule.Rule1.class)");
            System.out.println(objectMapper.writerWithView(JsonViewRule.Rule1.class).writeValueAsString(project));

            System.out.println();
            System.out.println("objectMapper.writerWithView(JsonViewRule.Rule2.class)");
            System.out.println(objectMapper.writerWithView(JsonViewRule.Rule2.class).writeValueAsString(project));
        } catch (IOException iOException) {
            iOException.printStackTrace();
        }
    }
}