package JSON;

import cococare.common.CCFieldConfig;
import com.fasterxml.jackson.annotation.JsonView;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Task {

    @JsonView({JsonViewRule.Rule2.class})
    @CCFieldConfig()
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}