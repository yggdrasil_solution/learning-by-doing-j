package POI;

import org.apache.poi.util.Units;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        DocxUtil docxUtil = new DocxUtil();
        docxUtil.open("D:\\OLD - Copy.docx");
        docxUtil.replaceText("${CUSTOMER_NAME!}", "Yosua Onesimus");
        docxUtil.replacePicture("${BARCODE!}", "D:\\google.png", Units.toEMU(200), Units.toEMU(200));
        docxUtil.save("D:\\RESULT.docx");
    }
}