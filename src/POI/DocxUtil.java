package POI;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class DocxUtil {

    private XWPFDocument document;

    public boolean open(OPCPackage package_) {
        try {
            document = new XWPFDocument(package_);
            return true;
        } catch (IOException iOException) {
            iOException.printStackTrace();
            return false;
        }
    }

    public boolean open(InputStream inputStream) {
        try {
            return open(OPCPackage.open(inputStream));
        } catch (InvalidFormatException invalidFormatException) {
            invalidFormatException.printStackTrace();
            return false;
        } catch (IOException iOException) {
            iOException.printStackTrace();
            return false;
        }
    }

    public boolean open(File file) {
        try {
            return open(new FileInputStream(file));
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
            return false;
        }
    }

    public boolean open(String pathName) {
        return open(new File(pathName));
    }

    public void replaceText(String parameter, String value) {
        for (XWPFParagraph paragraph : document.getParagraphs()) {
            String text = "";
            for (XWPFRun run : paragraph.getRuns()) {
                text += run.getText(0);
            }
            if (text.contains(parameter)) {
                text = text.replace(parameter, value);
                for (int i = 0; i < paragraph.getRuns().size(); i++) {
                    paragraph.getRuns().get(i).setText(i == 0 ? text : "", 0);
                }
            }
        }
    }

    public int getPictureType(String pictureName) {
        if (pictureName.endsWith(".emf")) {
            return XWPFDocument.PICTURE_TYPE_EMF;
        } else if (pictureName.endsWith(".wmf")) {
            return XWPFDocument.PICTURE_TYPE_WMF;
        } else if (pictureName.endsWith(".pict")) {
            return XWPFDocument.PICTURE_TYPE_PICT;
        } else if (pictureName.endsWith(".jpeg") || pictureName.endsWith(".jpg")) {
            return XWPFDocument.PICTURE_TYPE_JPEG;
        } else if (pictureName.endsWith(".png")) {
            return XWPFDocument.PICTURE_TYPE_PNG;
        } else if (pictureName.endsWith(".dib")) {
            return XWPFDocument.PICTURE_TYPE_DIB;
        } else if (pictureName.endsWith(".gif")) {
            return XWPFDocument.PICTURE_TYPE_GIF;
        } else if (pictureName.endsWith(".tiff")) {
            return XWPFDocument.PICTURE_TYPE_TIFF;
        } else if (pictureName.endsWith(".eps")) {
            return XWPFDocument.PICTURE_TYPE_EPS;
        } else if (pictureName.endsWith(".bmp")) {
            return XWPFDocument.PICTURE_TYPE_BMP;
        } else if (pictureName.endsWith(".wpg")) {
            return XWPFDocument.PICTURE_TYPE_WPG;
        } else {
            return -1;
        }
    }

    public boolean replacePicture(String parameter, InputStream inputStream, File file, int width, int height) {
        for (XWPFParagraph paragraph : document.getParagraphs()) {
            String text = "";
            for (XWPFRun run : paragraph.getRuns()) {
                text += run.getText(0);
            }
            if (text.contains(parameter)) {
                try {
                    for (int i = 0; i < paragraph.getRuns().size(); i++) {
                        XWPFRun run = paragraph.getRuns().get(i);
                        run.setText("", 0);
                        if (i == 0) {
                            run.addPicture(inputStream, getPictureType(file.getName()), file.getName(), width, height);
                        }
                    }
                } catch (InvalidFormatException invalidFormatException) {
                    invalidFormatException.printStackTrace();
                    return false;
                } catch (IOException iOException) {
                    iOException.printStackTrace();
                    return false;
                }
            }
        }
        return true;
    }

    public boolean replacePicture(String parameter, File file, int width, int height) {
        try {
            return replacePicture(parameter, new FileInputStream(file), file, width, height);
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
            return false;
        }
    }

    public boolean replacePicture(String parameter, String picturePath, int width, int height) {
        return replacePicture(parameter, new File(picturePath), width, height);
    }

    public boolean save(OutputStream outputStream) {
        try {
            document.write(outputStream);
            return true;
        } catch (IOException iOException) {
            iOException.printStackTrace();
            return false;
        }
    }

    public boolean save(File file) {
        try {
            return save(new FileOutputStream(file));
        } catch (FileNotFoundException fileNotFoundException) {
            fileNotFoundException.printStackTrace();
            return false;
        }
    }

    public boolean save(String pathName) {
        return save(new File(pathName));
    }
}