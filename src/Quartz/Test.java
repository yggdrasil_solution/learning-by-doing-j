package Quartz;

import cococare.common.CCFormat;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.impl.StdSchedulerFactory;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        try {
            Scheduler scheduler = new StdSchedulerFactory().getScheduler();
            scheduler.start();
            JobDetail job = new JobDetail();
            job.setName(MyJob.class.getSimpleName());
            job.setJobClass(MyJob.class);
            CronTrigger trigger = new CronTrigger();
            trigger.setName("trigger-" + MyJob.class.getSimpleName());
            trigger.setCronExpression(" * * 1 * * ?");//0 0 0 * * 0
            trigger.setStartTime(CCFormat.getDate("2017-06-06 00:00:00", "yyyy-MM-dd HH:mm:ss"));
//            trigger.setEndTime(CCFormat.getDate("2018-06-05 11:55:45", "yyyy-MM-dd HH:mm:ss"));
            scheduler.scheduleJob(job, trigger);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
