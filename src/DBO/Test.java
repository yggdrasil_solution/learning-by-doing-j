package DBO;

import cococare.database.CCDatabase;
import cococare.database.CCDatabaseConfig;
import cococare.database.CCDatabaseConfig.SupportedDatabase;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        CCDatabase database = new CCDatabase();

        CCDatabaseConfig databaseConfig = new CCDatabaseConfig();
        databaseConfig.setSupportedDatabase(SupportedDatabase.MySQLInnoDB);
        databaseConfig.setHost("sql6.freemysqlhosting.net");
        databaseConfig.setPort("3306");
        databaseConfig.setUsername("sql691064");
        databaseConfig.setPassword("nP6*fH3*");
        databaseConfig.setDatabase("sql691064");
        try {
            System.out.println(database.getConnection(databaseConfig, false));
        } catch (Exception exception) {
        }

        databaseConfig = new CCDatabaseConfig();
        databaseConfig.setSupportedDatabase(SupportedDatabase.MySQLInnoDB);
        databaseConfig.setHost("db4free.net");
        databaseConfig.setPort("3306");
        databaseConfig.setUsername("yosuaonesimus");
        databaseConfig.setPassword("06061984");
        databaseConfig.setDatabase("coco_trial");
        try {
            System.out.println(database.getConnection(databaseConfig, false));
        } catch (Exception exception) {
        }
    }
}