package SMB;

import java.net.MalformedURLException;
import jcifs.smb.NtlmPasswordAuthentication;
import jcifs.smb.SmbFile;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class SmbUtil {

    public static NtlmPasswordAuthentication newNtlmPasswordAuthentication(String domain, String username, String password) {
        return new NtlmPasswordAuthentication(domain, username, password);
    }

    public static SmbFile newSmbFile(NtlmPasswordAuthentication ntlmPasswordAuthentication, String ip, String path) {
        try {
            String url = "smb://" + ip + "/" + path + "/";
            return new SmbFile(url, ntlmPasswordAuthentication);
        } catch (MalformedURLException malformedURLException) {
            malformedURLException.printStackTrace();
            return null;
        }
    }

    public static SmbFile newSmbFile(String domain, String username, String password, String ip, String path) {
        if (domain.indexOf("\\") > 0) {
            domain = domain.split("\\\\")[0];
        }
        if (username.indexOf("\\") > 0) {
            username = username.split("\\\\")[1];
        }
        return newSmbFile(newNtlmPasswordAuthentication(domain, username, password), ip, path);
    }
}