package SMB;

import cococare.datafile.CCDom;
import java.io.IOException;
import jcifs.smb.SmbException;
import jcifs.smb.SmbFile;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        try {
            String domain = "domain";
            String username = "Yosua Onesimus";
            String password = "6684";
            String ip = "127.0.0.1";
            String path = "/Shared/";
            SmbFile smbFile = SmbUtil.newSmbFile(domain, username, password, ip, path);
            System.out.println(smbFile.exists());
            System.out.println(smbFile.getPath());
            System.out.println(smbFile.isDirectory());
            if (smbFile.isDirectory()) {
                for (SmbFile sf : smbFile.listFiles()) {
                    System.out.println(sf.getName());

                }
            }
            System.out.println(smbFile.isFile());
            if (smbFile.isFile()) {
                CCDom dom = new CCDom();
                try {
                    dom.read(smbFile.getInputStream());
                    dom.transform(null);
                } catch (IOException iOException) {
                    iOException.printStackTrace();
                }
            }
        } catch (SmbException smbException) {
            smbException.printStackTrace();
        }
    }
}