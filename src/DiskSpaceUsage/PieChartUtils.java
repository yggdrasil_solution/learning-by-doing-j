package DiskSpaceUsage;

import DiskSpaceUsage.FileProperty.DirectoryType;
import java.awt.Color;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardPieSectionLabelGenerator;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class PieChartUtils {

    public JFreeChart createPieChartDiskSpaceUsage(
            String title, File directory,
            boolean legend, boolean tooltips, boolean urls) {
        List<FileProperty> fileProperties = new FileProperty(directory).getChildren(1);

        DefaultPieDataset dataset = new DefaultPieDataset();
        for (FileProperty fileProperty : fileProperties) {
            dataset.setValue(fileProperty.getFilePath(), fileProperty.getSize());
        }

        JFreeChart jFreeChart = ChartFactory.createPieChart(
                title, dataset,
                legend, tooltips, urls);

        PiePlot plot = (PiePlot) jFreeChart.getPlot();
        plot.setLabelGenerator(new StandardPieSectionLabelGenerator(
                "{0}:\n({1} bytes, {2})", new DecimalFormat("0"), new DecimalFormat("0%")));

        HashMap<DirectoryType, Color> mapColor = new HashMap();
        for (DirectoryType directoryColor : DirectoryType.values()) {
            mapColor.put(directoryColor, directoryColor.getColor());
        }

        for (FileProperty fileProperty : fileProperties) {
            for (DirectoryType directoryColor : DirectoryType.values()) {
                if (fileProperty.getFilePath().contains(directoryColor.toString())) {
                    plot.setSectionPaint(fileProperty.getFilePath(), mapColor.put(directoryColor, DirectoryType.newColor(mapColor.get(directoryColor))));
                    plot.setExplodePercent(fileProperty.getFilePath(), directoryColor.getExplodePercent());
                    break;
                }
            }
        }

        return jFreeChart;
    }

    public JFreeChart createPieChartDiskSpaceUsage(
            String title, String pathname,
            boolean legend, boolean tooltips, boolean urls) {
        return createPieChartDiskSpaceUsage(
                title, new File(pathname),
                legend, tooltips, urls);
    }

    public boolean saveChartAsPNG(
            String title, File directory, File pngFile,
            boolean legend, boolean tooltips, boolean urls) {
        try {
            System.out.println("saveChartAsPNG() " + pngFile.getPath());
            JFreeChart jFreeChart = createPieChartDiskSpaceUsage(
                    title, directory,
                    legend, tooltips, urls);
            ChartUtilities.saveChartAsPNG(pngFile, jFreeChart, 800, 600);
            return true;
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return false;
        }
    }

    public boolean saveChartAsPNG(
            String title, String pathname, String pngPathname,
            boolean legend, boolean tooltips, boolean urls) {
        return saveChartAsPNG(
                title, new File(pathname), new File(pngPathname),
                legend, tooltips, urls);
    }
}