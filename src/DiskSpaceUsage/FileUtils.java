package DiskSpaceUsage;

import java.text.DecimalFormat;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class FileUtils {

    public static String readableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    public static void main(String[] args) {
        System.out.println(readableFileSize(1000));
        System.out.println(readableFileSize(1023));
        System.out.println(readableFileSize(1024));
        System.out.println(readableFileSize(1025));
        System.out.println(readableFileSize(1000000));
    }
}