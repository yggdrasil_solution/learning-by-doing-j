package DiskSpaceUsage;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class FileProperty {

    public enum DirectoryType {

        TRUNK("trunk", Color.GREEN, 0.05),
        BRANCH("branch", Color.CYAN, 0.05),
        TAG("tag", Color.PINK, 0.0),
        OTHER("other", Color.GRAY, 0.0);
        public static final double FACTOR = 0.9;
        private String string;
        private Color color;
        private double explodePercent;

        private DirectoryType(String string, Color color, double explodePercent) {
            this.string = string;
            this.color = color;
            this.explodePercent = explodePercent;
        }

        public Color getColor() {
            return color;
        }

        public static Color newColor(Color color) {
            return new Color(Math.max((int) (color.getRed() * FACTOR), 0),
                    Math.max((int) (color.getGreen() * FACTOR), 0),
                    Math.max((int) (color.getBlue() * FACTOR), 0));
        }

        public Color newColor() {
            return newColor(color);
        }

        public double getExplodePercent() {
            return explodePercent;
        }

        @Override
        public String toString() {
            return string;
        }
    }
    private File file;
    private Long totalSize;
    private Long otherSize;
    private boolean hasSubDir = false;

    public FileProperty(File file) {
        this.file = file;
    }

    public void refresh() {
        totalSize = null;
        otherSize = null;
    }

    public File getFile() {
        return file;
    }

    public String getFilePath() {
        if (hasSubDir) {
            return file.getPath() + " (other)";
        } else {
            return file.getPath();
        }
    }

    public long getTotalSize() {
        if (totalSize == null) {
            totalSize = FileUtils.sizeOfDirectory(this.file);
        }
        return totalSize;
    }

    public long getOtherSize() {
        if (otherSize == null) {
            otherSize = 0L;
            for (File dir : file.listFiles()) {
                if (dir.isDirectory()) {
                    otherSize += FileUtils.sizeOfDirectory(dir);
                }
            }
        }
        return getTotalSize() - otherSize;
    }

    public long getSize() {
        if (hasSubDir) {
            return getOtherSize();
        } else {
            return getTotalSize();
        }
    }

    private List<FileProperty> _getChildren(FileProperty fileProperty, int depth) {
        List<FileProperty> fileProperties = new ArrayList();
        for (File dir : fileProperty.getFile().listFiles()) {
            if (dir.isDirectory()) {
                fileProperty.hasSubDir = true;
                FileProperty fp = new FileProperty(dir);
                if (depth > 0) {
                    fileProperties.addAll(_getChildren(fp, depth - 1));
                }
                if (fp.getSize() > 0) {
                    fileProperties.add(fp);
                }
            }
        }
        return fileProperties;
    }

    public List<FileProperty> getChildren(int depth) {
        List<FileProperty> fileProperties = _getChildren(this, depth);
        if (getSize() > 0) {
            fileProperties.add(this);
        }
        return fileProperties;
    }
}