package DiskSpaceUsage;

import java.awt.Dimension;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        PieChartUtils pieChartUtils = new PieChartUtils();
        if (true) {
            JFreeChart jFreeChart = pieChartUtils.createPieChartDiskSpaceUsage(
                    "Disk Space Usage", "D:\\svn", false, true, false);
            ChartPanel chartPanel = new ChartPanel(jFreeChart);
            chartPanel.setPreferredSize(new Dimension(800, 600));
            ApplicationFrame applicationFrame = new ApplicationFrame("Frame Title");
            applicationFrame.setContentPane(chartPanel);
            applicationFrame.pack();
            RefineryUtilities.centerFrameOnScreen(applicationFrame);
            applicationFrame.setVisible(true);
        } else {
            pieChartUtils.saveChartAsPNG(
                    "Disk Space Usage", "D:\\svn", "D:\\svn.png",
                    false, true, false);
        }
    }
}