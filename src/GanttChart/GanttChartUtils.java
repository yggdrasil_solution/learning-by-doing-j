package GanttChart;

import java.awt.Color;
import java.awt.Paint;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.renderer.category.GanttRenderer;
import org.jfree.data.gantt.Task;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class GanttChartUtils {

    @SuppressWarnings("serial")
    private class CustomGanttRenderer extends GanttRenderer {

        private TaskSeriesCollection taskSeriesCollection;
        private int row = 0;
        private int column = 0;
        private int index = 0;
        private HashMap<String, Color> mapColor;

        public CustomGanttRenderer(
                TaskSeriesCollection taskSeriesCollection, HashMap<String, Color> mapColor) {
            this.taskSeriesCollection = taskSeriesCollection;
            this.mapColor = mapColor;
        }

        @Override
        @SuppressWarnings("unchecked")
        public Paint getItemPaint(int row, int col) {
            try {
                if (this.row != row || this.column != col) {
                    index = 0;
                    this.row = row;
                    this.column = col;
                }
                TaskSeries series = (TaskSeries) taskSeriesCollection.getRowKeys().get(row);
                List<Task> tasks = series.getTasks();
                if (tasks.get(col).getSubtaskCount() > 0) {
                    Task task = tasks.get(col).getSubtask(index / 2);
                    index++;
                    return mapColor.get(task.getDescription());
                } else {
                    return super.getItemPaint(row, col);
                }
            } catch (Exception exception) {
                exception.printStackTrace();
                return super.getItemPaint(row, col);
            }
        }
    }
    private TaskSeriesCollection taskSeriesCollection = new TaskSeriesCollection();
    private TaskSeries taskSeries;
    private Task task;

    private Date date(int date, int month, int year) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(year, month, date);
        return calendar.getTime();
    }

    public TaskSeries newTaskSeries(String name) {
        taskSeries = new TaskSeries(name);
        taskSeriesCollection.add(taskSeries);
        return taskSeries;
    }

    public Task newTask(String name, Date dateBegin, Date dateEnd) {
        task = new Task(name, dateBegin, dateEnd);
        taskSeries.add(task);
        return task;
    }

    public Task newTask(String name, int dateBegin, int monthBegin, int yearBegin, int dateEnd, int monthEnd, int yearEnd) {
        return newTask(name, date(dateBegin, monthBegin, yearBegin), date(dateEnd, monthEnd, yearEnd));
    }

    public Task newSubtask(String name, Date dateBegin, Date dateEnd) {
        Task subtask = new Task(name, dateBegin, dateEnd);
        task.addSubtask(subtask);
        return subtask;
    }

    public Task newSubtask(String name, int dateBegin, int monthBegin, int yearBegin, int dateEnd, int monthEnd, int yearEnd) {
        return newSubtask(name, date(dateBegin, monthBegin, yearBegin), date(dateEnd, monthEnd, yearEnd));
    }

    public JFreeChart createGanttChart(
            String title, String categoryAxisLabel, String dateAxisLabel,
            boolean legend, boolean tooltips, boolean urls,
            HashMap<String, Color> mapColor) {
        JFreeChart freeChart = ChartFactory.createGanttChart(
                title, categoryAxisLabel, dateAxisLabel, taskSeriesCollection,
                legend, tooltips, urls);

        CategoryPlot categoryPlot = (CategoryPlot) freeChart.getPlot();
        categoryPlot.getDomainAxis().setCategoryMargin(0.05);
        categoryPlot.getDomainAxis().setLowerMargin(0.05);
        categoryPlot.getDomainAxis().setUpperMargin(0.05);
        categoryPlot.setRenderer(new CustomGanttRenderer(taskSeriesCollection, mapColor));
        GanttRenderer renderer = (GanttRenderer) categoryPlot.getRenderer();
        renderer.setItemMargin(0);

        return freeChart;
    }

    public boolean saveChartAsPNG(
            String title, String categoryAxisLabel, String dateAxisLabel,
            boolean legend, boolean tooltips, boolean urls,
            HashMap<String, Color> mapColor, String fileName) {
        try {
            File file = new File(System.getProperty("java.io.tmpdir"), fileName + ".png");
            System.out.println("saveChartAsPNG() " + file.getPath());
            JFreeChart freeChart = createGanttChart(
                    title, categoryAxisLabel, dateAxisLabel,
                    legend, tooltips, urls,
                    mapColor);
            ChartUtilities.saveChartAsPNG(file, freeChart, 600, 400);
            return true;
        } catch (IOException ioException) {
            ioException.printStackTrace();
            return false;
        }
    }
}