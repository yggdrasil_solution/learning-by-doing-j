package GanttChart;

import java.awt.Color;
import java.util.Calendar;
import java.util.HashMap;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

/**
 * @author Yosua Onesimus
 * @since 13.03.17
 * @version 13.03.17
 */
public class Test {

    public static void main(String[] args) {
        GanttChartUtils ganttChartUtils = new GanttChartUtils();
        ganttChartUtils.newTaskSeries("Scheduled");
        ganttChartUtils.newTask("Project A", 1, Calendar.JANUARY, 2015, 31, Calendar.MARCH, 2015);
        ganttChartUtils.newSubtask("DEV", 1, Calendar.JANUARY, 2015, 10, Calendar.JANUARY, 2015);
        ganttChartUtils.newSubtask("VIT", 11, Calendar.JANUARY, 2015, 20, Calendar.JANUARY, 2015);
        ganttChartUtils.newSubtask("UAT", 21, Calendar.JANUARY, 2015, 30, Calendar.JANUARY, 2015);
        ganttChartUtils.newTask("Project B", 5, Calendar.JANUARY, 2015, 31, Calendar.MARCH, 2015);
        ganttChartUtils.newSubtask("DEV", 5, Calendar.JANUARY, 2015, 10, Calendar.FEBRUARY, 2015);
        ganttChartUtils.newSubtask("VIT", 10, Calendar.FEBRUARY, 2015, 20, Calendar.FEBRUARY, 2015);
        ganttChartUtils.newSubtask("UAT", 20, Calendar.FEBRUARY, 2015, 25, Calendar.FEBRUARY, 2015);

        HashMap<String, Color> mapColor = new HashMap();
        mapColor.put("DEV", Color.RED);
        mapColor.put("VIT", Color.YELLOW);
        mapColor.put("UAT", Color.GREEN);

        JFreeChart freeChart = ganttChartUtils.createGanttChart(
                "Project Timeline", "Project", "Date",
                false, false, false,
                mapColor);

        ChartPanel chartPanel = new ChartPanel(freeChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        ApplicationFrame applicationFrame = new ApplicationFrame("Frame Title");
        applicationFrame.setContentPane(chartPanel);
        applicationFrame.pack();
        RefineryUtilities.centerFrameOnScreen(applicationFrame);
        applicationFrame.setVisible(true);
    }
}